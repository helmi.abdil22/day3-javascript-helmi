function process(price, drink, money, email) {
  msg = `Welcome ${email} to to May's Vending Machine\n`;
  if (money - price >= 0) {
    msg += `Your choice is ${drink}, here's your drink\n`;
    msg += `Your changes are ${money - price}\n`;
  } else {
    msg += `Sorry, insufficient balance we can't process your ${drink}\n`;
    msg += `You need ${(money - price) * -1} more to buy this drink\n`;
  }
  msg += "Thank you";
  return msg;
}

function vendingMachine(email, money, drinkChoice = "coffee") {
  if (!email && !money) return "Please check your input";
  if (!email) return "Sorry can't process it until your email filled";
  if (!money) return "Sorry can't process it until you put your money";
  if (typeof money !== "number" || typeof email !== "string")
    return "Invalid input";

  switch (drinkChoice) {
    case "mineral water":
      return process(5000, "mineral water", money, email);
    case "cola":
      return process(7500, "cola", money, email);
    case "coffee":
      return process(12250, "coffee", money, email);
  }
}

console.log("Case 1\n" + vendingMachine("", 30000)); //case 1, output: Sorry can't process it until your email filled
console.log("Case 2\n" + vendingMachine(true, 30000)); //case 2, output: Invalid input
console.log("Case 3\n" + vendingMachine("g2academy@mail.com", 0)); //case 3, output: Sorry can't process it until you put your money
console.log("Case 4\n" + vendingMachine("g2academy@mail.com", "10000")); //case 4, output: Invalid input
console.log("Case 5\n" + vendingMachine("g2academy@mail.com", 20000)); //case 5, output: as expected
console.log("Case 6\n" + vendingMachine("g2academy@mail.com", 5000, "cola")); //case 6, output: as expected
